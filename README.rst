JuggleWipy
==========

Interactive Juggling Balls in micropython (wipy2.0 specifically)

*WIP*

Documentation
-------------

* https://arthur.lutz.im/blog/2017/12/balles-de-jonglage-avec-micropython-part-1/

Materials
---------

.. image:: materials.svg

Setup
-----

Example configuration can be used ::

  cp config.py config.py.example


Dev
---

The pymaker extension can be used in VScode or atom to develop the software. Take a look at pymakr.conf.example ::

  cp pymakr.conf.example pymakr.conf
  edit pymakr.conf

When developping with MQTT you can use mosquitto to watch the topics ::

  mosquitto_sub -t "#" -v

Dependencies
------------

* https://github.com/kfricke/micropython-usyslog/ - usyslog
* https://github.com/pycom/pycom-libraries/ - deepsleep
* https://github.com/micropython/micropython-lib/ - umqtt
