import machine
from network import WLAN
from config import configs


def connect():
    wlan = WLAN(mode=WLAN.STA)
    nets = wlan.scan()
    connected = False
    for config in configs:
        for net in nets:
            if net.ssid == config['ssid']:
                print('Network found! {}'.format(net.ssid))
                if config['auth']:
                    wlan.connect(net.ssid,
                                 auth=(net.sec, config['auth']),
                                 timeout=5000)
                else:
                    wlan.connect(net.ssid, timeout=5000)
                while not wlan.isconnected():
                    machine.idle()  # save power while waiting
                print('WLAN connection succeeded!')
                connected = True
            if connected:
                break
        if connected:
            break
    print(wlan.ifconfig())
    print(wlan.ifconfig())
    return wlan.ifconfig()
