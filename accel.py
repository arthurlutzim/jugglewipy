from machine import I2C
#
i2c = I2C(0)                         # create on bus 0
i2c = I2C(0, I2C.MASTER)             # create and init as a master
i2c.init(I2C.MASTER, baudrate=20000) # init as a master
##i2c.deinit()                         # turn off the peripheral
print(i2c.scan())

print(i2c.readfrom_mem(0x6A, 0x0,1))
