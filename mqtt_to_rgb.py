import machine  # noqa
import ubinascii
import time
import pycom  # noqa

from umqtt.simple import MQTTClient

from config import configs

config = configs[0] # TODO select appropriate config
DEVICES = {}

class PrintLogger:
    def debug(self, msg):
        print(msg)


try:
    import usyslog
    s = usyslog.UDPClient(ip=config['syslog_server'])
except OSError:
    s = PrintLogger()


def error_blip():
    pycom.rgbled(0x7f0000)  # red
    time.sleep(.1)
    pycom.rgbled(0x000000)  # off

id = ubinascii.hexlify(machine.unique_id()).decode('utf-8')
client = MQTTClient("pycom{}".format(id), config['mqtt_server'],
                    user=config['mqtt_user'],
                    password=config['mqtt_password'], port=1883)
colors_by_nb_of_devices = {
    1: 0x7f0000,
    2: 0x7f7f00,
    3: 0x007f00,
}

def sub_cb(topic, msg):
    s.debug('[wipy] [mqtt] {} {}'.format(topic, msg))
    if topic == b'pycom/unique_id':
        if msg not in DEVICES:
            DEVICES[msg] = True
            pycom.rgbled(colors_by_nb_of_devices[len(DEVICES)])
            client.publish(topic="pycom/unique_id", msg="{}".format(id))
    else:
        try:
            pycom.rgbled(int(msg))
        except ValueError:
            error_blip()

def setup():
    pycom.heartbeat(False)
    # startup lights
    pycom.rgbled(0x7f0000)  # red
    time.sleep(.5)
    pycom.rgbled(0x7f7f00)  # yellow
    time.sleep(.5)
    pycom.rgbled(0x007f00)  # green
    time.sleep(.5)
    pycom.rgbled(0x000000)  # off

def run(wlan_info):
    '''
    main run

    :wlan_info: information from wlan.ifconfig()
    '''
    setup()


    s.debug('[wipy] [mqtt] set_callback')
    client.set_callback(sub_cb)

    s.debug('[wipy] [mqtt] connect')
    client.connect()

    client.publish(topic="pycom/unique_id", msg="{}".format(id))
    sub_cb(b'pycom/unique_id', bytes(id, 'utf-8'))
    client.publish(topic="pycom/wlan_info", msg="{}".format(wlan_info))

    topic = config['balls'].get("pycom{}".format(id))
    if topic:
        s.debug('[wipy] [mqtt] subscribe {}'.format(topic))
        client.subscribe(topic=topic)

    client.subscribe(topic='pycom/unique_id')

    while True:
        # s.debug('[wipy] [mqtt] wait')
        client.check_msg()
        # time.sleep(.1)

