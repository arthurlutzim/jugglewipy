import pycom
import time
pycom.heartbeat(False)

# for red in range(0,125):
#     color = (red << 16) + (12 << 8)
#     pycom.rgbled(color)
#     time.sleep(0.1)
# for green in range(125,0):
#     pycom.rgbled((125 << 16) + (green << 8))
#     time.sleep(0.1)
# pycom.rgbled(0)

def green_to_red(value, min, avg, max, intensity=125):
    if value <= min:
        return 0,intensity,0
    elif value <= avg:
        return int((value-min)*intensity./(avg-min)),intensity,0
    elif value <= max:
        return intensity,int(intensity-((value-avg)*(intensity./(max-avg)))),0
    else:
        return intensity,0,0

for i in range(0,3000):
    print(i)
    red, green, blue = green_to_red(i, 200, 450, 2000)
    pycom.rgbled((red << 16) + (green << 8) + blue)
pycom.rgbled(0)

# for cycles in range(2): # stop after 2 cycles
#     pycom.rgbled(0x007f00) # green
#     time.sleep(1)
#     pycom.rgbled(0x7f7f00) # yellow
#     time.sleep(1)
#     pycom.rgbled(0x7f0000) # red
#     time.sleep(1)
