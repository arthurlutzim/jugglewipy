from umqtt.simple import MQTTClient
import machine
import time
from config import user, password

def sub_cb(topic, msg):
   print(msg)

client = MQTTClient("pycom", "io.adafruit.com",
                    user=user,
                    password=password, port=1883)

print('set_callback')
client.set_callback(sub_cb)
print('connect')
client.connect()
print('subscribe')
client.subscribe(topic="{}/feeds/lights".format(user))

while True:
    print("Sending ON")
    client.publish(topic="{}/feeds/lights".format(user), msg="ON")
    time.sleep(1)
    print("Sending OFF")
    client.publish(topic="{}/feeds/lights".format(user), msg="OFF")
    time.sleep(1)
