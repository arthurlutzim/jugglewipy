# main.py -- put your code here!
import usyslog

from setup_wifi import connect
from config import configs
from mqtt_to_rgb import run


def main():
    wlan_info = connect()
    config = configs[0]
    s = usyslog.UDPClient(ip=config['syslog_server'])
    s.debug('wipy: loaded wifi config, connected, usyslog message')
    run(wlan_info)


main()
